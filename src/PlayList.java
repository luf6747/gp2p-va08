import java.util.LinkedList;

import java.util.Collections;

import java.io.FileWriter;
import java.io.File;
import java.util.Scanner;

import java.io.IOException;

public class PlayList extends LinkedList<AudioFile> {
    private int current = 0; // default: start at the beginning (0)
    private boolean randomOrder = false; // default: no random order

    PlayList() {
    }

    PlayList(String pathname) {
        this();
        loadFromM3U(pathname);
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public AudioFile getCurrentAudioFile() {
        AudioFile tmp = null;
        if (super.size() > current && current >= 0) {
            tmp = super.get(current);
        }

        return tmp;
    }

    public void changeCurrent() {
        if (super.size() > current && current >= 0) {
            // if current is valid, do one step in ring:
            current = (current + 1) % super.size();

            if (randomOrder == true && current == 0) {
                Collections.shuffle(this);
            }
        } else {
            // else if current is invalid, reset to 0
            current = 0;
        }
    }

    public void setRandomOrder(boolean randomOrder) {
        this.randomOrder = randomOrder;
        if (this.randomOrder == true) {
            Collections.shuffle(this);
        } else {
            // no shuffle
        }
    }

    public void saveAsM3U(String pathname) {
        // write playlist to *.m3u file

        FileWriter writer = null;
        String linesep = System.getProperty("line.separator");

        try {
            writer = new FileWriter(pathname);

            for (int current = 0; current < super.size(); current++) {
                String tmp_str = get(current).getPathname();
                writer.write("" + tmp_str + linesep);
            }

        } catch (IOException e) {
            throw new RuntimeException("Unable to write to file" + pathname + ":" + e.getMessage());
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
                // NOP
            }
        }
    }

    public void loadFromM3U(String pathname) {
        this.clear(); // delete current playlist, space for new one
        this.setCurrent(0); // start with first song again
        Scanner scanner = null;

        try {
            scanner = new Scanner(new File(pathname));
            while (scanner.hasNextLine()) {
                String line_puffer;
                AudioFile Audiofile_ptr;
                line_puffer = scanner.nextLine();

                if (line_puffer.startsWith("#") == false && line_puffer.replace("\t", "").trim().isEmpty() == false) {
                    // line should be ok now, add it to playlist:
                    Audiofile_ptr = AudioFileFactory.getInstance(line_puffer);
                    this.addLast(Audiofile_ptr);
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                scanner.close();
            } catch (Exception e) {
                // NOP
            }

        }
    }
}
