import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.*;

public class UTestAudioFileFactory {

    @Test
    public void test_getInstance_00() throws Exception {
        try {
            AudioFileFactory.getInstance("unknown.xxx");
            fail("Unknown suffix; expected exception");
        } catch (RuntimeException e) {
            // expected
        }
    }

    @Test
    public void test_getInstance_01() throws Exception {
        try {
            AudioFileFactory.getInstance("nonexistent");
            fail("File is not readable; expecting exception");
        } catch (RuntimeException e) {
            // expected
        }
    }

    @Test
    public void test_getInstance_03() throws Exception {
        AudioFile af1 = AudioFileFactory.getInstance("audiofiles/Eisbach Deep Snow.ogg");
        assertTrue("Expecting object of type TaggedFile", (af1 instanceof TaggedFile));
        AudioFile af2 = AudioFileFactory.getInstance("audiofiles/wellenmeister - tranquility.wav");
        assertTrue("Expecting object of type WavFile", (af2 instanceof WavFile));
        AudioFile af3 = AudioFileFactory.getInstance("audiofiles/special.oGg");
        assertTrue("Expecting object of type TaggedFile", (af3 instanceof TaggedFile));
    }

}
