
public class AudioFileFactory {
    public static AudioFile getInstance(String pathname) {
        boolean error = false;
        AudioFile result = null;
        if (pathname.length() == 0 || pathname.contains(".") == false) {
            error = true;
        } else {
            int dot = pathname.lastIndexOf(".");
            String extension = pathname.substring(dot + 1).toLowerCase();
            if (extension.equals("wav")) {
                result = new WavFile(pathname);
            } else if (extension.equals("mp3") || extension.equals("ogg")) {
                result = new TaggedFile(pathname);
            } else {
                error = true;
            }
        }

        if (error) {
            throw new RuntimeException("Unknow suffix for AudioFile: \"" + pathname + "\"");
        } else {
            return result;
        }
    }
}
